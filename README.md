# PostgreSQL cluster on Docker
Set up an environment that contains a PostgreSQL cluster in separate containers

## Description
Deployment is done via Docker Swarm on 3 different hosts.

After initializing a Docker Swarm cluster, the HA PostgreSQL cluster is deployed via

```sh
docker deploy -c 'docker-stack.test.yml' pg-cluster
```

After deploying, check services via

```sh
docker service ls
```
